# Debian conda image with build-essential installed.
#
# This image needs rebuilt every time (non-Python) build dependencies are changed.
#
# Sean Leavey
# <sean.leavey@ligo.org>

FROM igwn/base:conda

LABEL name="Finesse Debian conda environment image" \
      maintainer="Sean Leavey <sean.leavey@ligo.org>"

RUN apt update
RUN apt install -y build-essential
RUN apt-get autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*