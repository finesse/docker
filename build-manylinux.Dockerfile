# Manylinux image with SuiteSparse installed.
#
# This image needs rebuilt every time (non-Python) build dependencies are changed.
#
# Sean Leavey
# <sean.leavey@ligo.org>

FROM quay.io/pypa/manylinux2014_x86_64

LABEL name="Finesse manylinux build image" \
      maintainer="Sean Leavey <sean.leavey@ligo.org>"

RUN yum install -y suitesparse-devel
RUN yum clean all && rm -rf /var/cache/yum