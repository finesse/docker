# Debian image with Sphinx dependencies installed for building the Finesse PDF docs.
#
# This image needs rebuilt every time (non-Python) build dependencies are changed.
#
# Sean Leavey
# <sean.leavey@ligo.org>

FROM sphinxdoc/sphinx-latexpdf

LABEL name="Finesse Debian Sphinx PDF documentation image" \
      maintainer="Sean Leavey <sean.leavey@ligo.org>"

RUN apt update
RUN apt install -y texlive-latex-base librsvg2-bin
RUN apt-get autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*