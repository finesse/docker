# Debian image with SuiteSparse installed, for debug builds and tests.
#
# This image needs rebuilt every time (non-Python) build dependencies are changed.
#
# Sean Leavey
# <sean.leavey@ligo.org>

FROM python:3.9

LABEL name="Finesse Debian debug image" \
      maintainer="Sean Leavey <sean.leavey@ligo.org>"

RUN apt update
RUN apt install -y libsuitesparse-dev
RUN apt-get autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*